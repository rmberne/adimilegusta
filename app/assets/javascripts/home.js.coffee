# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  $(".btn_minus").click ->
    ajax_appointment("/appointments/by_user/minus/", $(this))
  $(".btn_plus").click ->
     ajax_appointment("/appointments/by_user/plus/", $(this))

ajax_appointment = (base_url, $element) ->
     url = base_url + $element.data("coffee-type-id")
     $.post(url, (data) ->
       $element.parents("tr:first").find("td:nth-child(2)").html(data)
     ).fail ->
       alert("An error has occurred, try it again")      
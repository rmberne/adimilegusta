class AppointmentsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_appointment, only: [:show, :edit, :update, :destroy]

  # GET /appointments
  # GET /appointments.json
  def index
    @total = Appointment.count
    @page = params["page"]  ? params["page"] : 1
    @appointments = Appointment.paginate(:page => @page)
    @pages = (@total.to_f / WillPaginate.per_page).ceil
  end
  
  # GET /appointments/by_user/current
  # GET /appointments/by_user/current.json
  def by_current_user
    @total = Appointment.where(user_id: current_user.id).count
    @page = params["page"]  ? params["page"] : 1
    @appointments = Appointment.where(user_id: current_user.id).paginate(:page => @page)
    @pages = (@total.to_f / WillPaginate.per_page).ceil
    render "index"
  end
  
  # GET /appointments/by_user/plus/1
  # GET /appointments/by_user/plus/1.json
  def plus_one_by_current_user
    do_appointment(1)
  end
  
  # GET /appointments/by_user/minus/1
  # GET /appointments/by_user/minus/1.json
  def minus_one_by_current_user
    do_appointment(-1)
  end
  
  # Helper method for minus_one_by_user and plus_one_by_user
  def do_appointment(amount_param)
    appointment = Appointment.new({:coffee_type_id => params[:coffee_type_id], :user_id => current_user.id, :amount => amount_param})
    if appointment.save
      render :text => Appointment.where("coffee_type_id = ? and user_id = ?", params[:coffee_type_id], current_user.id).sum(:amount), :status => 200, :content_type => "plain/text"
    else
      render :nothing => true, :status => 500, :content_type => "plain/text"
    end
  end

  # GET /appointments/1
  # GET /appointments/1.json
  def show
  end

  # GET /appointments/new
  def new
    @appointment = Appointment.new
  end

  # GET /appointments/1/edit
  def edit
  end

  # POST /appointments
  # POST /appointments.json
  def create
    @appointment = Appointment.new(appointment_params)

    respond_to do |format|
      if @appointment.save
        format.html { redirect_to @appointment, notice: 'Appointment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @appointment }
      else
        format.html { render action: 'new' }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /appointments/1
  # PATCH/PUT /appointments/1.json
  def update
    respond_to do |format|
      if @appointment.update(appointment_params)
        format.html { redirect_to @appointment, notice: 'Appointment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /appointments/1
  # DELETE /appointments/1.json
  def destroy
    @appointment.destroy
    respond_to do |format|
      format.html { redirect_to appointments_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appointment
      @appointment = Appointment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def appointment_params
      params.require(:appointment).permit(:coffee_type_id, :user_id, :amount)
    end
end

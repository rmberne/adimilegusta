class HomeController < ApplicationController
  before_filter :authenticate_user!

  def index
    @coffee_balances = CoffeeType.joins("left outer join appointments on coffee_types.id = appointments.coffee_type_id and appointments.user_id = " + current_user.id.to_s).group("coffee_types.id").sum("appointments.amount")
  end
end

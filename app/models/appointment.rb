# Table Appointments
# Columns
# => coffee_type_id
# => user_id
# => amount
class Appointment < ActiveRecord::Base
  # Relationships
	belongs_to :coffee_type
	belongs_to :user
	
	validates_presence_of :coffee_type_id
	validates_presence_of :user_id
	validates_numericality_of :amount, :only_integer => true, :allow_nil => false
end

class CoffeeType < ActiveRecord::Base
   has_many :appointments, dependent: :destroy
   has_many :users, through: :appointments
   
   validates_presence_of :name
end

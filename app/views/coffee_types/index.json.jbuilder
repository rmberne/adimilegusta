json.array!(@coffee_types) do |coffee_type|
  json.extract! coffee_type, :id, :name
  json.url coffee_type_url(coffee_type, format: :json)
end

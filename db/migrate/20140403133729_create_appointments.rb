class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :coffee_type_id
      t.integer :user_id
      t.integer :amount

      t.timestamps
    end
  end
end

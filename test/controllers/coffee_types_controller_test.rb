require 'test_helper'

class CoffeeTypesControllerTest < ActionController::TestCase
  setup do
    @coffee_type = coffee_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:coffee_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create coffee_type" do
    assert_difference('CoffeeType.count') do
      post :create, coffee_type: { name: @coffee_type.name }
    end

    assert_redirected_to coffee_type_path(assigns(:coffee_type))
  end

  test "should show coffee_type" do
    get :show, id: @coffee_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @coffee_type
    assert_response :success
  end

  test "should update coffee_type" do
    patch :update, id: @coffee_type, coffee_type: { name: @coffee_type.name }
    assert_redirected_to coffee_type_path(assigns(:coffee_type))
  end

  test "should destroy coffee_type" do
    assert_difference('CoffeeType.count', -1) do
      delete :destroy, id: @coffee_type
    end

    assert_redirected_to coffee_types_path
  end
end
